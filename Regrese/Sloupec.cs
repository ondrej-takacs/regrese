﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regrese
{
    class Sloupec
    {
        public enum Sloupce : int {
             Stimulus = 2,
             ExportEndTime = 4,
             Participant = 5,
             Category = 9,
             Index = 11,
             StartTime = 12,
             EndTime = 13,
             Duration = 16,
             X = 17,
             Y = 18,
             SaccadeStartX = 24,
             SaccadeEndX = 26
        }

        public string GetData(string[] radek, Sloupce poradi)
        {
            return radek[(int)poradi];
        }
    }
}
