﻿using System.Reflection;

namespace Regrese
{
    class RadekVstup
    {
        private bool jeFixace = false;
        private bool jeInic = false;
        private string participant = "";
        private string sekvence = "";
        private int index = 0;
        private KategorieRadku kategorie = null;
        private decimal x = 0;
        private decimal y = 0;
        private decimal start = 0;
        private decimal konec = 0;
        private decimal trvani = 0;
        private decimal casCelkem = 0;

        private decimal sakadaStartX = 0;
        private decimal sakadaKonecX = 0;
        private bool jeSakadaZpetna = false;
        private bool jeSakada = false;

        public bool JeFixace { get => jeFixace; set => jeFixace = value; }
        public bool JeInic { get => jeInic; set => jeInic = value; }
        public string Participant { get => participant; set => participant = value; }
        public string Sekvence { get => sekvence; set => sekvence = value; }
        public int Index { get => index; set => index = value; }
        internal KategorieRadku Kategorie { get => kategorie; set => kategorie = value; }
        public decimal X { get => x; set => x = value; }
        public decimal Y { get => y; set => y = value; }
        public decimal Start { get => start; set => start = value; }
        public decimal Konec { get => konec; set => konec = value; }
        public decimal Trvani { get => trvani; set => trvani = value; }
        public decimal CasCelkem { get => casCelkem; set => casCelkem = value; }

        public decimal SakadaStartX { get => sakadaStartX; set => sakadaStartX = value; }
        public decimal SakadaKonecX { get => sakadaKonecX; set => sakadaKonecX = value; }
        public bool JeSakadaZpetna { get => jeSakadaZpetna; set => jeSakadaZpetna = value; }
        public bool JeSakada { get => jeSakada; set => jeSakada = value; }

        public RadekVstup(string radek)
        {
            var sloup = new Sloupec();
            var radekData = radek.Split(Zpracovani.Oddelovac);
            Kategorie = new KategorieRadku(sloup.GetData(radekData, Sloupec.Sloupce.Category));
            JeFixace = (Kategorie.ToString() == KategorieRadku.FIXACE) ?
                true
                : false;
            JeSakada = (Kategorie.ToString() == KategorieRadku.SAKADA) ?
                true
                : false;
            JeInic = true;
            Participant = sloup.GetData(radekData, Sloupec.Sloupce.Participant);
            Sekvence = sloup.GetData(radekData, Sloupec.Sloupce.Stimulus);

            if (Kategorie.ToString() == KategorieRadku.FIXACE)
            {
                Index = int.Parse(sloup.GetData(radekData, Sloupec.Sloupce.Index));
                X = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.X));
                Y = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.Y));
                Start = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.StartTime));
                Konec = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.EndTime));
                Trvani = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.Duration));
                CasCelkem = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.ExportEndTime));
            } else if (Kategorie.ToString() == KategorieRadku.SAKADA)
            {
                Index = int.Parse(sloup.GetData(radekData, Sloupec.Sloupce.Index));
                SakadaStartX = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.SaccadeStartX));
                SakadaKonecX = decimal.Parse(sloup.GetData(radekData, Sloupec.Sloupce.SaccadeEndX));
                jeSakadaZpetna = sakadaStartX > SakadaKonecX;
            }
        }

        public RadekVstup() { }

        public override string ToString()
        {
            string vysledek = "";
            PropertyInfo[] props = GetType().GetProperties();
            foreach (var property in props)
            {
                vysledek += property.Name + ": " + property.GetValue(this).ToString() + Zpracovani.Oddelovac;
            }
            return vysledek;
        }
    }
}
