﻿using System;

namespace Regrese
{
    class KategorieRadku
    {
        string Hodnota;
        public const string FIXACE = "Fixation";
        public const string SAKADA = "Saccade";
        public const string SEPARATOR = "Separator";
        public const string HLAVICKA = "Category";
        public const string MRKNUTI = "Blink";

        public KategorieRadku(string kategorie)
        {
            //if (kategorie == FIXACE
            //    || kategorie == SAKADA
            //    || kategorie == SEPARATOR
            //    || kategorie == HLAVICKA
            //    || kategorie == MRKNUTI)
            //{
                Hodnota = kategorie;
            //}
            //else
            //{
            //    throw new Exception("Nerozpoznaná kategorie: " + kategorie);
            //}
        }

        public override string ToString()
        {
            return Hodnota;
        }
    }
}
