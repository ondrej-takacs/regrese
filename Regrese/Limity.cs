﻿namespace Regrese
{
    class Limity
    {
        private decimal minSakada = 45;
        private decimal maxMala = 135;
        private decimal radiusKonceRereadingu = 12;
        private decimal skokNaRadekOd = 37;
        private decimal skokFuzzyOd = 20;
        private decimal skokFuzzyDo = 60;

        public decimal LimitRegrese { get => minSakada; set => minSakada = value; }
        public decimal MaxMala { get => maxMala; set => maxMala = value; }
        public decimal RadiusKonceRereadingu { get => radiusKonceRereadingu; set => radiusKonceRereadingu = value; }
        public decimal SkokNaRadekOd { get => skokNaRadekOd; set => skokNaRadekOd = value; }
        public decimal SkokFuzzyOd { get => skokFuzzyOd; set => skokFuzzyOd = value; }
        public decimal SkokFuzzyDo { get => skokFuzzyDo; set => skokFuzzyDo = value; }
    }
}
