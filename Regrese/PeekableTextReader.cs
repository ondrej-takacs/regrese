﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Regrese
{
    public class PeekableTextReader : TextReader
    {
        private StreamReader _Underlying;
        private Queue<string> BufferedLines;

        public PeekableTextReader(StreamReader underlying)
        {
            _Underlying = underlying;
            BufferedLines = new Queue<string>();
        }

        public string PeekLine()
        {
            string line = _Underlying.ReadLine();
            if (line == null)
                return null;
            BufferedLines.Enqueue(line);
            return line;
        }


        public override string ReadLine()
        {
            if (BufferedLines.Count > 0)
                return BufferedLines.Dequeue();
            return _Underlying.ReadLine();
        }

        public override void Close()
        {
            _Underlying.Close();
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _Underlying.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                TryDispose(ref _Underlying);
            }
        }

        private void TryDispose<T>(ref T obj) where T : class, IDisposable
        {
            try
            {
                if (obj != null)
                {
                    obj.Dispose();
                }
            }
            catch (Exception) { }
            obj = null;
        }

        public override bool Equals(object obj)
        {
            return _Underlying.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _Underlying.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return _Underlying.InitializeLifetimeService();
        }
    }
}