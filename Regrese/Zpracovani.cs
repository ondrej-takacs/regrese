﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Regrese
{
    class Zpracovani
    {
        int DebugLevel = 1;
        bool VypisRegreseUvnitrRegrese = false;
        bool VypisPrvniFuzzy = false;

        public static char Oddelovac = '\t';

        bool PreskakovatKontrolniRegrese = true;
        decimal KontrolniRegreseMezera = 30;
        decimal KontrolniRegreseTrva1Min = 250;
        decimal KontrolniRegreseTrva2Min = 250;
        decimal KontrolniRegreseTrva21Min = 200;

        decimal PredskokMinTrvani = 250;

        decimal DoskokMinPrvniVlevo = 150;
        decimal DoskokMinPrvniDolu = 20;

        decimal LimitRegrese = 45;
        decimal MaxMala = 135;
        decimal RadiusKonceRereadingu = 12;

        int MinPocetRereadingFixaci = 3;
        decimal MinDelkaRereadingSekvence = 170;

        static Dictionary<char, Dictionary<int, Limity>> LimitySeznam = new Dictionary<char, Dictionary<int, Limity>>()
        {
            {'A', new Dictionary<int, Limity>() {
                {1, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 50,    MaxMala = 165, SkokNaRadekOd = 37, SkokFuzzyOd = 35, SkokFuzzyDo = 40 } },
                {2, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 37, SkokFuzzyOd = 35, SkokFuzzyDo = 40 } },
                {3, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 50,    MaxMala = 165, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {4, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 50,    MaxMala = 165, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {5, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 40, SkokFuzzyOd = 35, SkokFuzzyDo = 50 } },
                {6, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {7, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                }
            },
            {'B', new Dictionary<int, Limity>() {
                {1, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 57.5m, MaxMala = 170, SkokNaRadekOd = 50, SkokFuzzyOd = 40, SkokFuzzyDo = 50 } },
                {2, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 50,    MaxMala = 165, SkokNaRadekOd = 37, SkokFuzzyOd = 35, SkokFuzzyDo = 40 } },
                {3, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {4, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 37, SkokFuzzyOd = 35, SkokFuzzyDo = 40 } },
                {5, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {6, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                {7, new Limity() { RadiusKonceRereadingu = 14, LimitRegrese = 47.5m, MaxMala = 150, SkokNaRadekOd = 55, SkokFuzzyOd = 50, SkokFuzzyDo = 60 } },
                }
            }
        };
        private Limity AktualniLimity;
        private bool JeVypsanoFuzzy = false;

        private string AktualniSekvence = "";

        private decimal DobaRegresiCelkem = 0;
        private decimal DobaMalychRegresiCelkem = 0;
        private decimal DobaVelkychRegresiCelkem = 0;
        private decimal DobaRereadinguCelkem = 0;
        private int PocetRegresiCelkem = 0;

        private decimal DobaCelkemParticipant = 0;
        private decimal DobaRegresiCelkemParticipant = 0;
        private decimal DobaMalychRegresiCelkemParticipant = 0;
        private decimal DobaVelkychRegresiCelkemParticipant = 0;
        private decimal DobaRereadinguCelkemParticipant = 0;
        private int PocetZpetnychFixaciCelkemParticipant = 0;
        private int PocetFixaciCelkemParticipant = 0;
        private int PocetSakadCelkemParticipant = 0;
        private int PocetZpetnychSakadCelkemParticipant = 0;
        private int PocetRegresiCelkemParticipant = 0;
        private Dictionary<string, Souhrny> SouhrnyParticipantu = new Dictionary<string, Souhrny>();

        private decimal DelkaVicecetnychRegresi = 0;

        private bool JeZacatekRadku = false;
        private StreamWriter Sw;

        private List<string> VypisPoznamky = new List<string>();
        private bool VicecetnaPoznamkaBylaPridana = false;

        private RadekVstup PredPredPredFixace = new RadekVstup();
        private Dictionary<int, RadekVstup> PrednacteneFixace = new Dictionary<int, RadekVstup>();

        public void Zpracuj(string vstupniSoubor, string vystupniSoubor)
        {
            Inicializace();

            NactiSouhrny(vstupniSoubor);

            using (PeekableTextReader sr = new PeekableTextReader(new StreamReader(vstupniSoubor)))
            {
                Sw = new StreamWriter(vystupniSoubor);
                Vypis("Respondent", "Text", "t zač", "t end", "t dur", "Large", "Small", "Reread",
                    "Index první regrese", "Index poslední regrese", "Notes");

                var sloup = new Sloupec();
                string r;
                var predPredFixace = new RadekVstup();
                var predchoziFixace = new RadekVstup();
                var fixace = new RadekVstup();
                var pristiFixace = new RadekVstup();
                var jeHledaniZacatku = true;
                var jeUvnitrRegrese = false;
                var zacatekRereadingu = new RadekVstup();
                var prvniNacteni = true;

                var zacatekRegrese = new RadekVstup();
                bool prirazenZacatek = false;

                decimal delkaRereadingu = 0;
                bool jeStejnyRadekRereading = true;

                decimal delkaRereadingSekvence = 0;
                int pocetRereadingFixaciSekv = 0;
                int maxDelkaSekvence = 0;

                while (pristiFixace.JeInic || prvniNacteni)
                {
                    r = sr.ReadLine();
                    var radekVstup = (r == null) ?
                        new RadekVstup()
                        : new RadekVstup(r);

                    if (!radekVstup.JeFixace && radekVstup.JeInic)
                    {
                        if (radekVstup.JeSakada)
                        {
                            PocetSakadCelkemParticipant++;
                            PocetZpetnychSakadCelkemParticipant += radekVstup.JeSakadaZpetna ? 1 : 0;
                        }
                        continue;
                    }

                    PredPredPredFixace = predPredFixace;
                    predPredFixace = predchoziFixace;
                    predchoziFixace = fixace;
                    fixace = pristiFixace;
                    pristiFixace = radekVstup;

                    PridejPrednactenouFixaci(PredPredPredFixace);
                    PridejPrednactenouFixaci(predPredFixace);
                    PridejPrednactenouFixaci(predchoziFixace);
                    PridejPrednactenouFixaci(fixace);
                    PridejPrednactenouFixaci(pristiFixace);

                    // pro inicializaci pristiRadek
                    if (prvniNacteni)
                    {
                        prvniNacteni = false;
                        continue;
                    }

                    PocetZpetnychFixaciCelkemParticipant += (fixace.X < predchoziFixace.X) ?
                        1 : 0;
                    PocetFixaciCelkemParticipant++;


                    // změna sekvence
                    if ((predchoziFixace.Sekvence != fixace.Sekvence && predchoziFixace.Sekvence != "")
                        || !pristiFixace.JeInic) // konec souboru
                    {
                        // uzavření reareadingu který nebyl dokončen
                        if (jeUvnitrRegrese)
                        {
                            VypisRegresi(zacatekRereadingu, predchoziFixace, predPredFixace, delkaRereadingu, jeStejnyRadekRereading,
                                delkaRereadingSekvence, maxDelkaSekvence, true, 0);
                        }

                        Vypis(predchoziFixace.Participant, predchoziFixace.Sekvence, "Celkové časy sekvence:", "",
                            DobaRegresiCelkem, DobaVelkychRegresiCelkem, DobaMalychRegresiCelkem, DobaRereadinguCelkem,
                            "Počet regresí sekvence:", PocetRegresiCelkem);

                        DobaCelkemParticipant += predchoziFixace.CasCelkem;
                        if (predchoziFixace.Participant != fixace.Participant
                            || !pristiFixace.JeInic) // konec souboru
                        {
                            var souhrn = SouhrnyParticipantu[predchoziFixace.Participant];
                            Vypis(predchoziFixace.Participant, "Celkové časy participanta:", "", "",
                                DobaRegresiCelkemParticipant, DobaVelkychRegresiCelkemParticipant, DobaMalychRegresiCelkemParticipant,
                                DobaRereadinguCelkemParticipant, "Čas celkem:", DobaCelkemParticipant,
                                "Počet fixací celkem:", souhrn.PocetFixaciCelkem,
                                "Počet zpětných fixací celkem:", souhrn.PocetZpetnychFixaciCelkem,
                                "Počet sakád celkem:", souhrn.PocetSakadCelkem,
                                "Počet zpětných sakád celkem:", souhrn.PocetZpetnychSakadCelkem,
                                "Počet regresí celkem:", PocetRegresiCelkemParticipant);
                                Vypis(predchoziFixace.Participant, "Poměry k celkovému času participanta:", "", "",
                                    DobaRegresiCelkemParticipant / DobaCelkemParticipant, DobaVelkychRegresiCelkemParticipant / DobaCelkemParticipant,
                                    DobaMalychRegresiCelkemParticipant / DobaCelkemParticipant, DobaRereadinguCelkemParticipant / DobaCelkemParticipant);

                            DobaRegresiCelkemParticipant = 0;
                            DobaMalychRegresiCelkemParticipant = 0;
                            DobaVelkychRegresiCelkemParticipant = 0;
                            DobaRereadinguCelkemParticipant = 0;
                            DobaCelkemParticipant = 0;
                            PocetZpetnychFixaciCelkemParticipant = 0;
                            PocetFixaciCelkemParticipant = 0;
                            PocetSakadCelkemParticipant = 0;
                            PocetZpetnychSakadCelkemParticipant = 0;
                            PocetRegresiCelkemParticipant = 0;
                        }

                        PredPredPredFixace = new RadekVstup();
                        predPredFixace = new RadekVstup();
                        predchoziFixace = new RadekVstup();
                        zacatekRereadingu = new RadekVstup();
                        PrednacteneFixace.Clear();

                        delkaRereadingu = 0;
                        jeUvnitrRegrese = false;
                        DobaRegresiCelkem = 0;
                        DobaMalychRegresiCelkem = 0;
                        DobaVelkychRegresiCelkem = 0;
                        DobaRereadinguCelkem = 0;
                        PocetRegresiCelkem = 0;
                        jeHledaniZacatku = true;
                        delkaRereadingSekvence = 0;
                        pocetRereadingFixaciSekv = 0;
                        maxDelkaSekvence = 0;
                        DelkaVicecetnychRegresi = 0;
                        prirazenZacatek = false;
                        JeVypsanoFuzzy = false;
                    }

                    NastavLimityPodleSekvence(fixace.Sekvence);

                    if (jeHledaniZacatku)
                    {
                        if (JeHledaniZacatku(fixace, predchoziFixace))
                        {
                            continue;
                        }
                        else
                        {
                            jeHledaniZacatku = false;
                        }
                    }

                    // počítání délky rereadingu
                    if (fixace.X > predchoziFixace.X)
                    {
                        delkaRereadingSekvence += fixace.X - predchoziFixace.X;
                        pocetRereadingFixaciSekv++;
                        if (pocetRereadingFixaciSekv > maxDelkaSekvence)
                        {
                            maxDelkaSekvence = pocetRereadingFixaciSekv;
                        }

                        //Vypis("DEBUG maxDelkaSekvence", maxDelkaSekvence);
                    }
                    else
                    {
                        pocetRereadingFixaciSekv = 0;
                    }

                    if (fixace.X < predchoziFixace.X)
                    {
                        DelkaVicecetnychRegresi += predchoziFixace.X - fixace.X;

                        if (!prirazenZacatek)
                        {
                            zacatekRegrese = predchoziFixace;
                            prirazenZacatek = true;
                        }
                    }
                    else
                    {
                        DelkaVicecetnychRegresi = 0;

                        zacatekRegrese = predchoziFixace;
                        prirazenZacatek = false;
                    }


                    // Detekce doskoků
                    if ((predchoziFixace.X - fixace.X) > DoskokMinPrvniVlevo // doleva
                        && (fixace.Y - predchoziFixace.Y) > DoskokMinPrvniDolu) // dolů
                    {
                        string nahled;
                        var fixaceNahled = new RadekVstup();
                        var predFixaceNahled = fixace;
                        int pocetNahleduFixaci = 0;
                        int prectenoRadku = 0;
                        while ((nahled = sr.PeekLine()) != null)
                        {
                            prectenoRadku++;
                            fixaceNahled = new RadekVstup(nahled);
                            if (!fixaceNahled.JeFixace)
                            {
                                continue;
                            }
                            if (predFixaceNahled.Sekvence != fixaceNahled.Sekvence)
                            {
                                break;
                            }


                            if (fixaceNahled.X > predFixaceNahled.X // vlevo
                                || fixaceNahled.Y < predFixaceNahled.Y) // nahoře
                            {
                                break;
                            }

                            pocetNahleduFixaci++;
                            predFixaceNahled = fixaceNahled;
                        }
                        if (pocetNahleduFixaci > 0)
                        {
                            // dohnání náhledů
                            var predX = -1;
                            var radekVstup2 = new RadekVstup();
                            while (prectenoRadku > 0)
                            {
                                var r2 = sr.ReadLine();
                                if (r2 == null)
                                {
                                    break;
                                }

                                radekVstup2 = new RadekVstup(r2);

                                if (radekVstup2.JeFixace)
                                {
                                    PocetZpetnychFixaciCelkemParticipant += (predX >= 0 && radekVstup2.X < predX) ? 1 : 0;
                                    PocetFixaciCelkemParticipant++;
                                }
                                else if (radekVstup2.JeSakada)
                                {
                                    PocetSakadCelkemParticipant++;
                                    PocetZpetnychSakadCelkemParticipant += radekVstup.JeSakadaZpetna ? 1 : 0;
                                }

                                prectenoRadku--;
                            }

                            fixace = predFixaceNahled;
                            pristiFixace = fixaceNahled;

                            VypisDebugLvl2($"Doskoky přes {pocetNahleduFixaci} fixací, " +
                                $"doskákáno z fixace s indexem {predchoziFixace.Index} na fixaci s indexem {fixace.Index}, " +
                                $"změna Y je {fixace.Y - predchoziFixace.Y}px");
                        }
                    }

                    // zpracování konce rereadingu
                    jeStejnyRadekRereading = jeStejnyRadekRereading && JeNaStejnemRadku(fixace.Y, predchoziFixace.Y);
                    FuzzyVarovani(fixace, predchoziFixace.Y, "(detekce konce rereadingu)");
                    if (jeUvnitrRegrese && JeKonecRereadingu(fixace.X, fixace.Y, zacatekRereadingu.X, zacatekRereadingu.Y))
                    {
                        // Konec je podle poslední fixace v radiusu => přeskočení na další fixaci v rádiusu
                        if (JeVRadiusu(zacatekRereadingu.X, pristiFixace.X))
                        {
                            //VypisDebug($"Příští fixace {pristiFixace.Index} je v rádiusu počáteční fixace {zacatekRereadingu.Index}");
                            continue;
                        }

                        VypisRegresi(zacatekRereadingu, fixace, predchoziFixace, delkaRereadingu, jeStejnyRadekRereading,
                                delkaRereadingSekvence, maxDelkaSekvence, false, 0);

                        //VypisDebug(sw, "x:", radek.X, "y:" , radek.Y, "zacX:", 
                        //    zacatekRereadingu.X, "zacY:", zacatekRereadingu.Y);

                        delkaRereadingu = 0;
                        zacatekRereadingu = new RadekVstup();
                        jeUvnitrRegrese = false;
                        zacatekRegrese = fixace;
                        prirazenZacatek = false;
                    }

                    // zpracování první regrese
                    if ((!jeUvnitrRegrese || VypisRegreseUvnitrRegrese)
                        && JeRegrese(fixace.X, predchoziFixace.X, fixace.Y, predchoziFixace.Y))
                    {
                        // detekce začátku řádku
                        if ((
                                JeZacatekRadku
                                || (JeNaVyssimRadku(predPredFixace.Y, predchoziFixace.Y)
                                    && predchoziFixace.X < predPredFixace.X)
                            ) && !JeNaVyssimRadku(fixace.Y, predchoziFixace.Y))
                        {
                            VypisRegresi(predchoziFixace, fixace, predchoziFixace, delkaRereadingu, jeStejnyRadekRereading,
                                        delkaRereadingSekvence, maxDelkaSekvence, false, 1,
                                        "Není regrese protože jde o hledání začátku řádku, " +
                                        "protože regrese začaly skokem o řádek níž a doleva");
                                        //$"Index aktuální fixace je: {fixace.Index}, předchozí fixace je: {predchoziFixace.Index}, fixace je výše o: {predchoziFixace.Y - fixace.Y}px");

                            JeZacatekRadku = true;
                            continue;
                        }

                        // skok dolů a zpět není regrese
                        if (JeNaDalsimRadku(predchoziFixace.Y, predPredFixace.Y)
                            && JeNaVyssimRadku(fixace.Y, predchoziFixace.Y))
                        {
                            VypisRegresi(predchoziFixace, fixace, predchoziFixace, delkaRereadingu, jeStejnyRadekRereading,
                                        delkaRereadingSekvence, maxDelkaSekvence, false, 1,
                                        "Není regrese protože jde o skok dolů a zpět");
                            continue;
                        }

                        delkaRereadingu += Math.Abs(predchoziFixace.X - fixace.X);
                        //VypisDebugLvl2("DEBUG delkaRereadingu", delkaRereadingu, "radek.X", radek.X,
                        //    "predchoziRadek.X", predchoziRadek.X);
                        //VypisDebug("Debug: predPred vyssi skok", radek.Index, predchoziRadek.Y, predPredRadek.Y, JeNaVyssimRadku(predPredRadek.Y, predchoziRadek.Y));

                        // přeskakování kontrolních regresí
                        // zpětná sakáda, která se nevrací dál než předchozí fixace (doplňují předtím vynechaná místa)
                        var rozdilPredPredX = fixace.X - predPredFixace.X;
                        var rozdilPristiX = pristiFixace.X - predchoziFixace.X;
                        FuzzyVarovani(fixace, predchoziFixace.Y, "(přeskakování kontrolních regresí)");
                        if (PreskakovatKontrolniRegrese && JeNaStejnemRadku(fixace.Y, predchoziFixace.Y)
                            && (rozdilPredPredX > KontrolniRegreseMezera
                                // předPřed je na předchozím řádku a tím splňuje X podmínku
                                || JeNaVyssimRadku(predPredFixace.Y, predchoziFixace.Y))
                            && rozdilPristiX > RadiusKonceRereadingu)
                        {
                            // Výjimka pro časově dělší fixace
                            if (predchoziFixace.Trvani > KontrolniRegreseTrva1Min)
                            {
                                VypisPoznamky.Add("Výjimka kontrolní regrese");
                                VypisPoznamky.Add($"fixace s indexem:{predchoziFixace.Index} trvá déle než {KontrolniRegreseTrva1Min}ms ({predchoziFixace.Trvani}ms)");
                            }
                            else if (fixace.Trvani > KontrolniRegreseTrva2Min
                              && predchoziFixace.Trvani > KontrolniRegreseTrva21Min)
                            {
                                VypisPoznamky.Add("Výjimka kontrolní regrese");
                                VypisPoznamky.Add($"fixace s indexem:{fixace.Index} trvá déle než {KontrolniRegreseTrva2Min}ms ({fixace.Trvani}ms)");
                                VypisPoznamky.Add($"fixace s indexem:{predchoziFixace.Index} trvá déle než {KontrolniRegreseTrva21Min}ms ({predchoziFixace.Trvani}ms)");
                            }
                            else
                            {
                                VypisRegresi(predchoziFixace, fixace, predchoziFixace, delkaRereadingu, jeStejnyRadekRereading,
                                    delkaRereadingSekvence, maxDelkaSekvence, false, 1, $"Není regrese protože:",
                                    $"1. fixace-1 s indexem:{predPredFixace.Index} je před fixací1 s indexem:{fixace.Index}"
                                      + $" více než {KontrolniRegreseMezera}px ({rozdilPredPredX}px)",
                                    $"2. fixace2 s indexem:{pristiFixace.Index} je za fixací0 s indexem:{predchoziFixace.Index}"
                                      + $" více než {RadiusKonceRereadingu}px ({rozdilPristiX}px)"
                                    );
                                continue;
                            }
                        }
                        else
                        {
                            //VypisDebugLvl2("DEBUG radek", radek.ToString());
                            //VypisDebugLvl2("DEBUG predchoziRadek", predchoziRadek.ToString());
                            //VypisDebugLvl2("DEBUG predPredRadek", predPredRadek.ToString());
                            //VypisDebugLvl2("DEBUG", "JeNaStejnemRadku:", JeNaStejnemRadku(radek.Y, predchoziRadek.Y),
                            // "predPredRadek.X < radek.X:", predPredRadek.X < radek.X,
                            // "JeNaVyssimRadku(predPredRadek.Y, predchoziRadek.Y):", JeNaVyssimRadku(predPredRadek.Y, predchoziRadek.Y));
                        }

                        if (VypisRegreseUvnitrRegrese)
                        {
                            //Vypis(radek.Participant, radek.Sekvence,
                            //    predchoziRadek.Konec, radek.Konec, radek.Konec - predchoziRadek.Konec,
                            //    "", "", "", predchoziRadek.Index, radek.Index, "VypisRegreseUvnitrRegrese",
                            //    "DelkaRereadingu:", delkaRereadingu, "jeStejnyRadekRereading:", jeStejnyRadekRereading);
                            VypisRegresi(predchoziFixace, fixace, predchoziFixace, delkaRereadingu, jeStejnyRadekRereading,
                                delkaRereadingSekvence, maxDelkaSekvence, false, 1, "RegreseUvnitrRegrese");
                        }

                        if (!jeUvnitrRegrese)
                        {
                            jeUvnitrRegrese = true;
                            zacatekRereadingu = zacatekRegrese;
                            jeStejnyRadekRereading = true;
                            delkaRereadingSekvence = 0;
                            pocetRereadingFixaciSekv = 0;
                            maxDelkaSekvence = 0;

                            // určení nového typu pro novou regresi
                            delkaRereadingu = Math.Abs(predchoziFixace.X - fixace.X);
                            VypisDebugLvl2("DEBUG", "delkaRereadingu nova", delkaRereadingu, "radek.X", fixace.X,
                            "predchoziRadek.X", predchoziFixace.X);
                            FuzzyVarovani(fixace, predchoziFixace.Y, "(detekce rereadingu)");
                            jeStejnyRadekRereading = JeNaStejnemRadku(fixace.Y, predchoziFixace.Y);
                        }
                    }
                    else
                    {
                        JeZacatekRadku = false;
                    }
                }
                Sw.Close();
            }

            //string RadekToString(string[] radekData)
            //{
            //    var sloup = new Sloupec();
            //    return Sloupec.Sloupce.Participant + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.Participant) + Oddelovac
            //            + Sloupec.Sloupce.Stimulus + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.Stimulus) + Oddelovac
            //            + Sloupec.Sloupce.Category + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.Category) + Oddelovac
            //            + Sloupec.Sloupce.Index + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.Index) + Oddelovac
            //            + Sloupec.Sloupce.StartTime + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.StartTime) + Oddelovac
            //            + Sloupec.Sloupce.EndTime + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.EndTime) + Oddelovac
            //            + Sloupec.Sloupce.X + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.X) + Oddelovac
            //            + Sloupec.Sloupce.Y + ":"
            //            + sloup.GetData(radekData, Sloupec.Sloupce.Y) + Oddelovac;
            //}
        }

        private void NactiSouhrny(string vstupniSoubor)
        {
            using (var sr = new StreamReader(vstupniSoubor))
            {
                int pocetZpetnychFixaciCelkemParticipant = 0;
                int pocetFixaciCelkemParticipant = 0;
                int pocetSakadCelkemParticipant = 0;
                int pocetZpetnychSakadCelkemParticipant = 0;
                // přeskočení řádku s názvy sloupců
                string r = sr.ReadLine();
                var radek = new RadekVstup();
                var predRadek = new RadekVstup();
                var predFixace = new RadekVstup();
                while ((r = sr.ReadLine()) != null)
                {
                    radek = new RadekVstup(r);

                    //if (predRadek.JeInic && predRadek.Sekvence != radek.Sekvence)
                    //{
                    //    Console.WriteLine($"{predRadek.Participant} {predRadek.Sekvence}_______________________________________");
                    //    Console.WriteLine($"pocetZpetnychFixaciCelkemParticipant: {pocetZpetnychFixaciCelkemParticipant}");
                    //    Console.WriteLine($"pocetZpetnychSakadCelkemParticipant: {pocetZpetnychSakadCelkemParticipant}");
                    //    pocetZpetnychFixaciCelkemParticipant = 0;
                    //    pocetFixaciCelkemParticipant = 0;
                    //    pocetSakadCelkemParticipant = 0;
                    //    pocetZpetnychSakadCelkemParticipant = 0;
                    //    Console.ReadKey();
                    //    break;
                    //}

                    if (predRadek.JeInic && predRadek.Participant != radek.Participant)
                    {
                        SouhrnyParticipantu.Add(predRadek.Participant,
                            new Souhrny() {
                                PocetZpetnychFixaciCelkem = pocetZpetnychFixaciCelkemParticipant,
                                PocetFixaciCelkem = pocetFixaciCelkemParticipant,
                                PocetZpetnychSakadCelkem = pocetZpetnychSakadCelkemParticipant,
                                PocetSakadCelkem = pocetSakadCelkemParticipant
                            });
                        pocetZpetnychFixaciCelkemParticipant = 0;
                        pocetFixaciCelkemParticipant = 0;
                        pocetSakadCelkemParticipant = 0;
                        pocetZpetnychSakadCelkemParticipant = 0;
                    }

                    if (radek.JeFixace)
                    {
                        pocetZpetnychFixaciCelkemParticipant += (predFixace.JeInic && radek.X < predFixace.X) ? 1 : 0;
                        //if ((predFixace.JeInic && radek.X < predFixace.X) && radek.Participant == "r470_A2")
                        //{
                        //    Console.WriteLine($"Zpetna fixace {radek.Index}");
                        //}
                        pocetFixaciCelkemParticipant++;
                        predFixace = radek;
                    }
                    else if (radek.JeSakada)
                    {
                        pocetSakadCelkemParticipant++;
                        pocetZpetnychSakadCelkemParticipant += radek.JeSakadaZpetna ? 1 : 0;
                        //if (radek.JeSakadaZpetna && radek.Participant == "r470_A2")
                        //{
                        //    Console.WriteLine($"Zpetna sakáda {radek.Index}");
                        //}
                    }

                    predRadek = radek;
                }
                SouhrnyParticipantu.Add(predRadek.Participant,
                    new Souhrny()
                    {
                        PocetZpetnychFixaciCelkem = pocetZpetnychFixaciCelkemParticipant,
                        PocetFixaciCelkem = pocetFixaciCelkemParticipant,
                        PocetZpetnychSakadCelkem = pocetZpetnychSakadCelkemParticipant,
                        PocetSakadCelkem = pocetSakadCelkemParticipant
                    });
            }
        }

        private void PridejPrednactenouFixaci(RadekVstup fixace)
        {
            if (fixace.JeInic)
            {
                if (PrednacteneFixace.ContainsKey(fixace.Index))
                {
                    PrednacteneFixace[fixace.Index] = fixace;
                } else
                {
                    PrednacteneFixace.Add(fixace.Index, fixace);
                }
            }
        }

        private void Inicializace()
        {
            JeVypsanoFuzzy = false;
            AktualniSekvence = "";
            DobaRegresiCelkem = 0;
            DobaMalychRegresiCelkem = 0;
            DobaVelkychRegresiCelkem = 0;
            DobaRereadinguCelkem = 0;
            DelkaVicecetnychRegresi = 0;
            PocetRegresiCelkem = 0;
            DobaRegresiCelkemParticipant = 0;
            DobaMalychRegresiCelkemParticipant = 0;
            DobaVelkychRegresiCelkemParticipant = 0;
            DobaRereadinguCelkemParticipant = 0;
            DobaCelkemParticipant = 0;
            PocetFixaciCelkemParticipant = 0;
            PocetZpetnychFixaciCelkemParticipant = 0;
            PocetSakadCelkemParticipant = 0;
            PocetZpetnychSakadCelkemParticipant = 0;
            PocetRegresiCelkemParticipant = 0;
            SouhrnyParticipantu.Clear();
            JeZacatekRadku = false;
            VypisPoznamky = new List<string>();
            VicecetnaPoznamkaBylaPridana = false;
            PredPredPredFixace = new RadekVstup();
            PrednacteneFixace.Clear();
        }

        private void NastavLimityPodleSekvence(string sekvence)
        {
            if (sekvence != "" && sekvence != AktualniSekvence)
            {
                AktualniSekvence = sekvence;
                var typ = sekvence[1];
                var cislo = int.Parse(sekvence[2].ToString());
                AktualniLimity = LimitySeznam[typ] [cislo];
                RadiusKonceRereadingu = AktualniLimity.RadiusKonceRereadingu;
                LimitRegrese = AktualniLimity.LimitRegrese;
                MaxMala = AktualniLimity.MaxMala;
                VypisDebug($"Limity přiřazené pro {typ}{cislo} jsou:", $"Radius: {RadiusKonceRereadingu}px",
                    $"Limit regrese: {LimitRegrese}px", $"Limit malé/velké regrese: {MaxMala}px",
                    $"SkokNaRadekOd: {AktualniLimity.SkokNaRadekOd}px", $"SkokFuzzyOd: {AktualniLimity.SkokFuzzyOd}px",
                    $"SkokFuzzyDo: {AktualniLimity.SkokFuzzyDo}px");
            }
        }

        private void VypisRegresi(RadekVstup radek1, RadekVstup radek2, RadekVstup predRadek,
            decimal delkaRereadingu, bool jeStejnyRadekRereading, decimal delkaRereadingSekvence,
            int maxDelkaSekvence, bool konecSekvence, int debug = 0, params object[] args)
        {
            if (DebugLevel >= 1 && JeNaVyssimRadku(radek1.Y, radek2.Y))
            {
                var predpredIndex = predRadek.Index - 1;
                if (predRadek.Trvani < PredskokMinTrvani && PrednacteneFixace.ContainsKey(predpredIndex))
                {
                    radek2 = PrednacteneFixace[predpredIndex];
                    // Fixace s nulovou hodnotou je třeba vypisovat jen pro kontrolu
                    if (radek1.Index == radek2.Index)
                    {
                        debug = 1;
                        VypisPoznamky.Add($"Počáteční i cílová fixace je shodná, proto se vypisuje jenom pro debugování");
                    }

                    VypisPoznamky.Add($"Regrese byla ukončena skokem na další řádek, " +
                        $"jako poslední se počítá fixace s indexem {radek2.Index}. " +
                        $"Předchozí fixace s indexem {predRadek.Index} byla přeskočena, " +
                        $"protože trvala méně než {PredskokMinTrvani}ms ({predRadek.Trvani}ms)");
                }
                else
                {
                    radek2 = predRadek;
                    VypisPoznamky.Add($"Regrese byla ukončena skokem na další řádek, " +
                        $"jako poslední se počítá fixace s indexem {radek2.Index}");
                }
            }

            decimal doba = 0;
            decimal konec = 0;
            if (konecSekvence || JeVRadiusu(radek1.X, radek2.X))
            {
                konec = radek2.Konec;
                if (DebugLevel >= 1)
                {
                    VypisPoznamky.Add($"Fixace {radek1.Index} je v radiusu {RadiusKonceRereadingu}px od fixace {radek2.Index}");
                }
            }
            else
            {
                konec = radek2.Start;
            }
            doba = konec - radek1.Konec;
            //VypisDebugLvl2("DEBUG Math.Abs(radek1.X - radek2.X) <= RadiusKonceRereadingu",
            //    Math.Abs(radek1.X - radek2.X) <= RadiusKonceRereadingu,
            //    "radek1.X", radek1.X, "radek2.X", radek2.X, "RadiusKonceRereadingu", RadiusKonceRereadingu);

            var dobaMala = GetDobaMala(delkaRereadingu, jeStejnyRadekRereading, doba);
            var dobaVelka = GetDobaVelka(dobaMala, doba);
            var dobaRereading = GetDobaRereading(delkaRereadingSekvence, maxDelkaSekvence, doba);

            if (debug == 0)
            {
                DobaRegresiCelkem += doba;
                DobaMalychRegresiCelkem += dobaMala;
                DobaVelkychRegresiCelkem += dobaVelka;
                DobaRereadinguCelkem += dobaRereading;

                DobaRegresiCelkemParticipant += doba;
                DobaMalychRegresiCelkemParticipant += dobaMala;
                DobaVelkychRegresiCelkemParticipant += dobaVelka;
                DobaRereadinguCelkemParticipant += dobaRereading;
                //VypisDebug($"Pričteno {doba} z fixace {radek1.Index}, doba regresí je {DobaRegresiCelkem}");
            }

            object[] parametry = {radek2.Participant, radek2.Sekvence, radek1.Konec, konec, doba,
                    dobaVelka, dobaMala, dobaRereading, radek1.Index, radek2.Index};
            if (DebugLevel >= debug)
            {
                if (debug > 0)
                {
                    Vypis(parametry, "DEBUG", args, VypisPoznamky.ToArray());
                }
                else
                {
                    PocetRegresiCelkem++;
                    PocetRegresiCelkemParticipant++;
                    Vypis(parametry, args, VypisPoznamky.ToArray());
                }
            }

            VypisPoznamky.Clear();
            VicecetnaPoznamkaBylaPridana = false;
            JeVypsanoFuzzy = false;
        }


        private void VypisDebug(params object[] args)
        {
            if (DebugLevel >= 1)
            {
                Vypis("DEBUG", args);
            }
        }

        private void VypisDebugLvl2(params object[] args)
        {
            if (DebugLevel >= 2)
            {
                Vypis("DEBUG", args);
            }
        }

        private void Vypis(params object[] args)
        {
            var vypis = "";
            foreach (object arg in args)
            {
                if (arg.GetType().IsArray)
                {
                    foreach (object a in (object[])arg)
                    {
                        vypis += a.ToString() + Oddelovac;
                    }
                } else
                {
                    vypis += arg.ToString() + Oddelovac;
                }
            }
            Sw.WriteLine(vypis);
        }

        private decimal GetDobaVelka(decimal dobaMala, decimal doba)
        {
            if (dobaMala == 0)
            {
                return doba;
            }
            return 0;
        }

        private decimal GetDobaRereading(decimal delkaRereadingSekvence, int maxDelkaSekvence, decimal doba)
        {
            //VypisDebugLvl2("DEBUG maxDelkaSekvence", maxDelkaSekvence, "delkaRereadingSekvence", delkaRereadingSekvence);
            if (maxDelkaSekvence >= MinPocetRereadingFixaci
                && delkaRereadingSekvence >= MinDelkaRereadingSekvence)
            {
                return doba;
            }
            return 0;
        }

        private decimal GetDobaMala(decimal delkaRereadingu, bool jeStejnyRadekRereading, decimal doba)
        {
            //VypisDebugLvl2("DEBUG jeStejnyRadekRereading", jeStejnyRadekRereading, "delkaRereadingu", delkaRereadingu);
            if (jeStejnyRadekRereading && delkaRereadingu <= MaxMala)
            {
                return doba;
            }
            return 0;
        }

        private bool JeHledaniZacatku(RadekVstup fixace, RadekVstup predchoziFixace)
        {
            if (predchoziFixace.Kategorie == null)
            {
                return true;
            } else if (fixace.X <= predchoziFixace.X
                && fixace.Y <= (predchoziFixace.Y + AktualniLimity.SkokNaRadekOd))
            {
                //VypisDebug($"Fixace {fixace.Index} se přeskakuje protože jde o hledání nového řádku");
                return true;
            } else
            {
                return false;
            }
        }

        private bool JeRegrese(decimal x, decimal predchoziX, decimal y, decimal predchoziY)
        {
            if (!VicecetnaPoznamkaBylaPridana && (predchoziX - x) <= LimitRegrese
                && DelkaVicecetnychRegresi > LimitRegrese)
            {
                VypisPoznamky.Add($"Je vícečetná regrese s délkou {DelkaVicecetnychRegresi}px která je větší než limit {LimitRegrese}px");
                VicecetnaPoznamkaBylaPridana = true;
            }
            return (DelkaVicecetnychRegresi > LimitRegrese
                && JeNaStejnemNeboVyssimRadku(y, predchoziY))
                // skok vpravo nahoru
                || (JeNaVyssimRadku(y, predchoziY));
        }

        private bool JeKonecRereadingu(decimal x, decimal y, decimal zacatekRereadinguX, decimal zacatekRereadinguY)
        {
            return ((x + RadiusKonceRereadingu) > zacatekRereadinguX && JeNaStejnemRadku(y, zacatekRereadinguY))
                || JeNaDalsimRadku(y, zacatekRereadinguY);
        }

        private bool JeVRadiusu(decimal x1, decimal x2)
        {
            return Math.Abs(x2 - x1) <= RadiusKonceRereadingu;
        }

        private bool JeNaStejnemRadku(decimal y, decimal predchoziY)
        {
            //VypisDebug(predchoziY, y, ToleranceRadek);
            return predchoziY >= (y - AktualniLimity.SkokNaRadekOd)
                && predchoziY <= (y + AktualniLimity.SkokNaRadekOd);
        }

        private bool JeNaStejnemNeboDalsimRadku(decimal y, decimal predchoziY)
        {
            return JeNaStejnemRadku(y, predchoziY) || y >= predchoziY;
        }

        private bool JeNaStejnemNeboVyssimRadku(decimal y, decimal predchoziY)
        {
            return JeNaStejnemRadku(y, predchoziY) || y <= predchoziY;
        }

        private bool JeNaVyssimRadku(decimal y, decimal predchoziY)
        {
            return !JeNaStejnemRadku(y, predchoziY) && y < predchoziY;
        }

        private bool JeNaDalsimRadku(decimal y, decimal predchoziY)
        {
            return !JeNaStejnemRadku(y, predchoziY) && y > predchoziY;
        }

        private bool JeFuzzyRadek(decimal y, decimal predchoziY)
        {
            var zmenaY = Math.Abs(y - predchoziY);
            return zmenaY <= AktualniLimity.SkokFuzzyDo && zmenaY >= AktualniLimity.SkokFuzzyOd;
        }

        private void FuzzyVarovani(RadekVstup fixace, decimal predchoziY, string varovani)
        {
            if (!JeVypsanoFuzzy && JeFuzzyRadek(fixace.Y, predchoziY))
            {
                VypisPoznamky.Add($"Fuzzy interval u fixace {fixace.Index} " + varovani);
                JeVypsanoFuzzy = VypisPrvniFuzzy;
            }
        }
    }
}
