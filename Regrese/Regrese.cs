﻿namespace Regrese
{
    /**
     * Testování: CTRL+ALT+A
     *   Tools.DiffFiles Regrese\data\data_vystup_A2_v5.txt Regrese\data\data_vystup_A2_debug.txt
     *   Tools.DiffFiles Regrese\data\data_vystup_A2_debug.txt Regrese\data\data_vystup_A2_debug2.txt
     */   
    class Regrese
    {
        static string CestaData = "../../data/";
        static string VstupniSouborPrefix = "SingleEventStatistics";
        static string VystupniSouborPrefix = "data_vystup";
        static string Pripona = "txt";
        static string[] Vstupy = {"A1", "A2", "B1", "B2"};
        static string Verze = "v6_3";
        //static string Konfigurace = "konfigurace.txt";
        static void Main(string[] args)
        {
            Zpracovani zpracovani = new Zpracovani();
            foreach (string vstup in Vstupy)
            {

                var vstupSouborNazev = $"{CestaData + VstupniSouborPrefix}_{vstup}.{Pripona}";
                var vystupSouborNazev = $"{CestaData + VystupniSouborPrefix}_{vstup}_{Verze}.{Pripona}";
                zpracovani.Zpracuj(vstupSouborNazev, vystupSouborNazev);
            }
            
        }
    }
}
